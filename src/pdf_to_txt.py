import pdfplumber
from urllib.request import urlopen
from shutil import copyfileobj

def pdf_print(url):
    """Retrieves text from pdf using pdf url"""
    with urlopen(url) as in_stream, open("ulotka.pdf", 'wb') as out_file:
        copyfileobj(in_stream, out_file)
    pdf = pdfplumber.open('ulotka.pdf')
    text = ""
    for i in range(1, len(pdf.pages)):
        page = pdf.pages[i]
        text += page.extract_text()
    pdf.close()
    return text