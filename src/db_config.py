import mysql.connector

class DBManager():
    """
    Object allowing for SQL database management

    Attributes:
        username (str): database username
        key (str): database key
    """
    def __init__(self, username, key):
        self.__username = username
        self.__key = key


    def __connection(self):
        """Returns a mysql connection object"""
        connector = mysql.connector.connect(
            host="db-health-app.cwjkshob0aso.eu-west-1.rds.amazonaws.com",
            user=self.__username,
            password=self.__key,
            database="health_app"
        )
        return connector

    
    @staticmethod
    def _check(entry: str, text: str, cursor) -> bool:
        cursor.execute('SELECT COUNT(*) FROM users WHERE ' + entry + '=%s;', [text])
        count_of_users = cursor.fetchone()[0]
        if count_of_users == 0:
            return True
        return False

    
    @staticmethod
    def check_user_password(username: str, password: str, cursor) -> bool:
        """Checks whether password is correct for given user"""
        cursor.execute("SELECT password FROM health_app.users WHERE username"+"=%s;", [username])
        correct_pass = cursor.fetchone()[0]
        if password == correct_pass:
            return True
        return False


    def add_user_to_db(self, username: str, password: str, email: str) -> bool:
        db = self.__connection()
        cursor = db.cursor()

        if self._check("username", username, cursor) and self._check("email", email, cursor):
            sql = "INSERT INTO health_app.users (username, `password`, email) VALUES (%s, %s, %s)"
            values = (username, password, email)
            cursor.execute(sql, values)
            db.commit()
            return True

        else: return False


    def log_in(self, username: str, password: str) -> bool:
        db = self.__connection()
        cursor = db.cursor()
        if not self._check("username", username, cursor) and not self._check("password", password, cursor) and self.check_user_password(username, password, cursor):
            return True
        return False


    def get_shelf(self, username: str):
        """Returns table of medicines for particular user"""
        pass

