import pickle

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.clock import mainthread
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import StringProperty
from db_config import DBManager
from scrape_drug_info import DrugInfoRetriever
from pdf_to_txt import pdf_print



class StartPage(Screen):
    pass


class LoginPage(Screen):
    db = DBManager(
            username="studentka",
            key="jsdc8kjn90cd."
        )
    def verify_user(self):
        return self.db.log_in(
            username=self.ids["nick"].text, 
            password=self.ids["password"].text
)


class RegisterPage(Screen):
    def verify_credentials(self):
        db = DBManager(
            username="studentka",
            key="jsdc8kjn90cd."
        )

        return db.add_user_to_db(
            username=self.ids["nick"].text, 
            password=self.ids["password"].text, 
            email=self.ids["mail"].text
            )


class HomePage(Screen):
    pass


class SettingsPage(Screen):
    pass


class CameraPage(Screen):
    def update(self):
        global label 
        label = self.ids["label"].text
        print(label) 


class SearchPage(Screen):

    def action(self, instance, value):
        word_list = pickle.load(open("files/list_of_names.pickle", "rb"))
        self.ids.nazwa.suggestion_text = ''
        val = value.lower()

        if not val:
            return
        try:
            word = [word for word in word_list
                    if word.lower().startswith(val)][0][len(val):]

            if not word:
                return
            self.ids.nazwa.suggestion_text = word
        except IndexError:
            print('Index Error.')

def change_screen(*args):
    App.get_running_app().root.current = 'drug'


class ResultsPage(Screen):
    def on_pre_enter(self, *args):
        self.create_buttons()

        
    def zrob(self, content):
        screen = self.manager.get_screen('drug')
        print(screen.ids.pdf_text.text)
        screen.ids.pdf_text.text = content
    

    @mainthread
    def create_buttons(self):

        screen = self.manager.get_screen('search')
        entry = screen.ids.nazwa.text
        is_gtin=False
        if entry == "":
            entry = label
            print(entry)
            is_gtin=True
        
        ret_info = DrugInfoRetriever(
            drug = entry, 
            is_gtin=is_gtin
            )

        html = ret_info.get_html()
        results = ret_info.get_search_results(html)
        for i in range(len(results)):
            item = results[i]
            button = Button(text=item["Nazwa, postać, dawka"]+ "\n" + item["Zawartość opakowania"])
            app = App.get_running_app()
            sm = app.root
            button.bind(on_press=lambda x: self.zrob(pdf_print(item["Adres pdf"])), on_release=lambda *args: setattr(sm, 'current', "drug"))
            self.ids.grid.add_widget(button)


class DrugPage(Screen):
    def ChangeText(self, content):
        self.ids.pdf_text.text = content

class MainApp(App):
    pass


if __name__=="__main__":
    MainApp().run()

