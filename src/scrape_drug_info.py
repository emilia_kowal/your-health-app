from mechanize import Browser
from bs4 import BeautifulSoup


class DrugInfoRetriever():
    """
    Object for scraping drug information from
    medicines registry

    Attributes:
        drug (str): drug name or GTIN number
        is_gtin (bool): is drug attr GTIN number
    """

    def __init__(self, drug: str, is_gtin: bool):
        self.drug = drug
        self.is_gtin = is_gtin

    
    def get_html(self):
        """
        Returns html of performed search for drug

        """
        br = Browser()
        br.addheaders = [('User-agent', 'Firefox')]
        br.open("http://leki.urpl.gov.pl/")
        br.select_form(nr=0)
        br.form['szukacz'] = self.drug

        if self.is_gtin:
            br['kategoria'] = ["EAN"]

        response = br.submit()
        soup = BeautifulSoup(
            response.read(), 'html.parser'
            )
        return soup

    def get_search_results(self, html):
        """
        Retrieves info from table extracted 
        from html for given drug
        
        Args: 
            html: html for given search

        Returns:
            list_of_dicts: rows of tables extracted from html stored as dict
        """
        table = html.find_all('table')[0]

        list_of_dicts = []
        for entry in table.find_all("tr")[1:]:
            prop = entry.find_all('td')
            dom = "http://leki.urpl.gov.pl"
            row = {
                "Substancja czynna": prop[1].get_text(),
                "Nazwa, postać, dawka": prop[2].get_text(),
                "Zawartość opakowania": prop[3].get_text(),
                "Adres pdf": dom + prop[5].find('img').get('onclick').split("'")[1]
            }
            list_of_dicts.append(row)
        return list_of_dicts