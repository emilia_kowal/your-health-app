# %%
import pickle
import pandas as pd

# %%
med = pd.read_excel("files/Produkty_lecznicze_Tue Jun 15 2021 13_25_59 GMT+0200 (Central European Summer Time).xlsx")
# %%
names = med['Nazwa Produktu Leczniczego'].unique()
# %%
with open("list_of_names.pickle", 'wb') as f:
    pickle.dump(names, f)
# %%
